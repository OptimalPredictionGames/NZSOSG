from common_imports import *
# python setup.py build_ext --inplace
from scipy.misc import derivative as scipy_derivative
from scipy.optimize import minimize, basinhopping


if __name__ == '__main__':
    import p1_routines, p2_routines

    def feasibility_penalty(a, b, derivative_roots):
        if (a < b) and (a <= derivative_roots[0]) and (b >= derivative_roots[1]):
            return 0.0
        else:
            return 1.0

    def p1_transformed_expected_threshold_reward(x, a, b, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        if x >= b:
            return 0.0
        if x <= a:
            f_x = p1_routines.stop_first_payoff(x, *p1_stop_first_payoff_args)
            g_b_x = p1_follower_transformed(x, b, *p1_stop_second_payoff_args)
            return f_x - g_b_x

        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        g_b_a = p1_follower_transformed(a, b, *p1_stop_second_payoff_args)
        return (f_a - g_b_a) * (b - x) / (b - a)


    def p2_transformed_expected_threshold_reward(y, a, b, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        if y <= a:
            return 0.0
        if y >= b:
            f_y = p2_routines.stop_first_payoff(y, *p2_stop_first_payoff_args)
            g_a_y = p2_follower_transformed(a, y, *p2_stop_second_payoff_args)
            return f_y - g_a_y
        f_b = p2_routines.stop_first_payoff(b, *p2_stop_first_payoff_args)
        g_a_b = p2_follower_transformed(a, b, *p2_stop_second_payoff_args)
        return (f_b - g_a_b) * (y - a) / (b-a)


    def p1_follower_transformed(a, b, *p1_stop_second_payoff_args):
        if a >= b:
            return p1_routines.stop_second_payoff(a, *p1_stop_second_payoff_args)
        return p1_routines.stop_second_payoff(b, *p1_stop_second_payoff_args) * (a/b)


    def p2_follower_transformed(a, b, *p2_stop_second_payoff_args):
        if a >= b:
            return p2_routines.stop_second_payoff(b, *p2_stop_second_payoff_args)
        return p2_routines.stop_second_payoff(a, *p2_stop_second_payoff_args) * ((1-b)/(1-a))


    def p1_utility_function(a, b, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        if a >= b:
            return -np.inf
        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        g_b_a = p1_follower_transformed(a, b, *p1_stop_second_payoff_args)
        utility_value = (f_a - g_b_a)/(b-a)
        return utility_value

    def p2_utility_function(a, b, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        if a >= b:
            return -np.inf
        f_b = p2_routines.stop_first_payoff(b, *p2_stop_first_payoff_args)
        g_a_b = p2_follower_transformed(a, b, *p2_stop_second_payoff_args)
        utility_value = (f_b - g_a_b)/(b-a)
        return utility_value

    def theta_function(a, b, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        d_f_a = scipy_derivative(p1_routines.stop_first_payoff, a, dx=dx, args=p1_stop_first_payoff_args)
        g_b = p1_routines.stop_second_payoff(b, *p1_stop_second_payoff_args)
        non_linear_value = f_a + (d_f_a*(b - a)) - g_b
        return non_linear_value

    def gamma_function(a, b, dx, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        f_b = p2_routines.stop_first_payoff(b, *p2_stop_first_payoff_args)
        d_f_b = scipy_derivative(p2_routines.stop_first_payoff, b, dx=dx, args=p2_stop_first_payoff_args)
        g_a = p2_routines.stop_second_payoff(a, *p2_stop_second_payoff_args)
        non_linear_value = f_b + (d_f_b*(a - b)) - g_a
        return non_linear_value


    def double_smooth_fit(guess, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args, p2_stop_first_payoff_args,
                            p2_stop_second_payoff_args, derivative_roots):
        a, b = guess
        value = theta_function(a, b, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args)**2
        value += gamma_function(a, b, dx, p2_stop_first_payoff_args, p2_stop_second_payoff_args)**2
        value += feasibility_penalty(a, b, derivative_roots)
        return value

    lower_boundary = 0.0
    upper_boundary = 1.0
    boundaries = [lower_boundary, upper_boundary]
    num_points = 10000
    dx = (upper_boundary - lower_boundary) / float(num_points)
    x_points = np.arange(lower_boundary, upper_boundary+dx, dx)

    # parameters for stop first payoff, player 1
    # cubic case
    second_derivative_slope, second_derivative_zero, constant_term = 10, 0.45, 0.0
    p1_stop_first_payoff_args = (second_derivative_slope, second_derivative_zero, constant_term)
    # add an extra non-negative quadratic term which vanishes at the boundaries
    # cubic case
    stop_second_extra_quad_coef = 0.25
    p1_stop_second_payoff_args = (second_derivative_slope, second_derivative_zero, constant_term, lower_boundary,
                               upper_boundary, stop_second_extra_quad_coef)

    p2_second_derivative_slope, p2_second_derivative_zero, p2_constant_term = 10, 0.55, 0.0
    p2_stop_first_payoff_args = (p2_second_derivative_slope, 1.0-p2_second_derivative_zero, p2_constant_term)

    p2_stop_second_constant_term = p2_constant_term
    p2_stop_second_extra_quad_coef = 0.25
    p2_stop_second_payoff_args = (p2_second_derivative_slope, 1.0-p2_second_derivative_zero, p2_stop_second_constant_term, lower_boundary,
                                  upper_boundary, p2_stop_second_extra_quad_coef)

    y_points = np.array([p1_routines.stop_first_payoff(x, *p1_stop_first_payoff_args) for x in x_points])
    p1_stop_second_points = np.array([p1_routines.stop_second_payoff(x, *p1_stop_second_payoff_args) for x in x_points])
    y2_points = np.array([p2_routines.stop_first_payoff(x, *p2_stop_first_payoff_args) for x in x_points])
    p2_stop_second_points = np.array([p2_routines.stop_second_payoff(x, *p2_stop_second_payoff_args) for x in x_points])

    derivative_roots = [second_derivative_zero, p2_second_derivative_zero]

    thresholds = np.array([1.0-0.7559102, 0.7559102], dtype=np.float64)

    doPlotRewardFunction = True
    doSaveRewardFunctionPlot = True

    doPlotUtilityFunction = True
    doSaveUtilityFunctionPlot = True

    doSolveGame = True
    doBasinHopping = True
    doPlotValueFunction = True
    doSaveValueFunctionPlot = True

    if doSolveGame:

        if doBasinHopping:
            result = basinhopping(double_smooth_fit, thresholds, niter=100000,
                                        minimizer_kwargs={
                                            'method': 'SLSQP',
                                            'bounds': [(lower_boundary, second_derivative_zero),
                                                       (p2_second_derivative_zero, upper_boundary)],
                                             'args': (dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args,
                                                      p2_stop_first_payoff_args,
                                                      p2_stop_second_payoff_args, derivative_roots),
                                             'options': {'ftol': 1e-12}
                                        }
                                  )
        else:
            result = minimize(double_smooth_fit, thresholds, method='L-BFGS-B',
                              bounds=[(lower_boundary,second_derivative_zero), (p2_second_derivative_zero, upper_boundary)],
                                args=(dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args, p2_stop_first_payoff_args,
                                p2_stop_second_payoff_args, derivative_roots), options={'ftol': 10.0})

        if not doBasinHopping:
            print "Success?: ", result.success
            if not result.success:
                print result.message
        thresholds = result.x
        print thresholds, result.fun

    if doPlotRewardFunction:
        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points, color='r', label=r"$x \mapsto f_{1}(x)$", #marker='*',
                markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, p1_stop_second_points, color='r', label=r"$x \mapsto g_{1}(x)$", #marker='o',
                linestyle='-.', markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, y2_points, color='b', label=r"$x \mapsto f_{2}(x)$", #marker='*',
                markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, p2_stop_second_points, color='b', label=r"$x \mapsto g_{2}(x)$", #marker='o',
                linestyle='-.', markevery=len(x_points) / 10, linewidth=2.0)

        ax.axvline(second_derivative_zero, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=1.5)

        ax.annotate(r"${a}$", xy=(second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveRewardFunctionPlot:
            plt.savefig('./results/reward_functions.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()

    if doPlotUtilityFunction:
        x, y = thresholds
        p1_follower_transformed_values = np.array(
            [p1_follower_transformed(x_, y, *p1_stop_second_payoff_args) for x_ in x_points])

        p1_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) <= max(second_derivative_zero, p2_second_derivative_zero))]
        p1_utilities = np.array(
            [p1_utility_function(x_, y, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in p1_state_space])
        p2_follower_transformed_values = np.array(
            [p2_follower_transformed(x, y_, *p2_stop_second_payoff_args) for y_ in x_points])
        p2_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) >= min(second_derivative_zero, p2_second_derivative_zero))]
        p2_utilities = np.array(
            [p2_utility_function(x, y_, p2_stop_first_payoff_args, p2_stop_second_payoff_args) for y_ in p2_state_space])

        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points - p1_follower_transformed_values, color='r', label=r"$x \mapsto f_{1}(x) - g_{1,[\hat{y},1]}(x)$",
                markevery=len(x_points) / 10, linewidth=2.0)

        ax.plot(p1_state_space, p1_utilities, color='r', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$x \mapsto U_{1}(x,\hat{y})$")

        ax.plot(x_points, y2_points - p2_follower_transformed_values, color='b', label=r"$y \mapsto f_{2}(y) - g_{2,[0,\hat{x}]}(y)$",
                markevery=len(x_points) / 10, linewidth=2.0)

        ax.plot(p2_state_space, p2_utilities, color='b', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$y \mapsto U_{2}(\hat{x},y)$")

        ax.axvline(x, linestyle=':', color='k', linewidth=1.5)
        ax.axvline(y, linestyle=':', color='k', linewidth=1.5)

        ax.annotate(r"${\hat{x}}$", xy=(x, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${\hat{y}}$", xy=(y, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.axvline(second_derivative_zero, linestyle='--', color='k', linewidth=2.0)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=2.0)

        ax.annotate(r"${a}$", xy=(second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveUtilityFunctionPlot:
            plt.savefig('./results/utility_functions.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()

    if doPlotValueFunction:

        x_star, y_star = thresholds

        p1_follower_transformed_values = np.array(
            [p1_follower_transformed(x_, y_star, *p1_stop_second_payoff_args) for x_ in x_points])

        p1_transformed_expected_threshold_reward_values = np.array([p1_transformed_expected_threshold_reward(x_, x_star, y_star, p1_stop_first_payoff_args, p1_stop_second_payoff_args)
                                                                    for x_ in x_points])

        p1_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) <= max(second_derivative_zero, 1 - p2_second_derivative_zero))]
        p1_utilities = np.array(
            [p1_utility_function(x_, y_star, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in
             p1_state_space])

        p2_follower_transformed_values = np.array(
            [p2_follower_transformed(x_star, y_, *p2_stop_second_payoff_args) for y_ in x_points])
        p2_transformed_expected_threshold_reward_values = np.array([p2_transformed_expected_threshold_reward(y_, x_star, y_star, p2_stop_first_payoff_args, p2_stop_second_payoff_args)
                                                                    for y_ in x_points])

        p2_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) >= min(second_derivative_zero, 1 - p2_second_derivative_zero))]
        p2_utilities = np.array(
            [p2_utility_function(x_star, y_, p2_stop_first_payoff_args, p2_stop_second_payoff_args) for y_ in
             p2_state_space])

        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points - p1_follower_transformed_values, color='r', label=r"$x \mapsto f_{1}(x) - g_{1,[y_{*},1]}(x)$",
                markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, p1_transformed_expected_threshold_reward_values, color='r', label=r"$x \mapsto \tilde{V}_{1}^{[y_{*},1]}(x)$",
                linestyle='--', markevery=len(x_points) / 10, linewidth=2.0)

        ax.plot(p1_state_space, p1_utilities, color='r', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$x \mapsto U_{1}(x,y_{*})$")

        ax.plot(x_points, y2_points - p2_follower_transformed_values, color='b',
                label=r"$y \mapsto f_{2}(y) - g_{2,[0,x_{*}]}(y)$",
                markevery=len(x_points) / 10, linewidth=2.0)

        ax.plot(x_points, p2_transformed_expected_threshold_reward_values, color='b', label=r"$y \mapsto \tilde{V}_{2}^{[0,x_{*}]}(y)$",
                linestyle='--', markevery=len(x_points) / 10, linewidth=2.0)

        ax.plot(p2_state_space, p2_utilities, color='b', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$y \mapsto U_{2}(x_{*},y)$")

        ax.axvline(x_star, linestyle=':', color='k', linewidth=2.0)
        ax.axvline(y_star, linestyle=':', color='k', linewidth=2.0)

        ax.annotate(r"${x_{*}}$", xy=(x_star, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${y_{*}}$", xy=(y_star, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.axvline(second_derivative_zero, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=1.5)

        ax.annotate(r"${a}$", xy=(second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveValueFunctionPlot:
            plt.savefig('./results/value_functions.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()