from common_imports import *
# python setup.py build_ext --inplace


if __name__ == '__main__':
    import p1_routines_non_termination as p1_routines
    import p2_routines_non_termination as p2_routines
    from textwrap import wrap

    lower_boundary = 0.0
    upper_boundary = 1.0
    boundaries = [lower_boundary, upper_boundary]
    num_points = 10000
    dx = (upper_boundary - lower_boundary) / float(num_points)
    x_points = np.arange(lower_boundary, upper_boundary+dx, dx)

    p1_steepness = 1500.0
    p1_impulse_point = 1.0/3.0

    p1_stop_first_payoff_args = (p1_impulse_point, p1_steepness)

    p1_apex_point = 2.0/3.0
    p1_apex_value = 2.0
    p1_stop_second_payoff_args = (p1_apex_point, p1_apex_value, lower_boundary, upper_boundary)

    p2_apex_point_1, p2_apex_value_1, p2_apex_point_2, p2_apex_value_2 = 1.0/3.0, -1.5, 2.0/3.0, -0.3

    p2_stop_first_payoff_args = (p2_apex_point_1, p2_apex_value_1, p2_apex_point_2, p2_apex_value_2, lower_boundary, upper_boundary)

    p2_stop_second_payoff_args = (p2_apex_point_1, p2_apex_value_1+0.2, p2_apex_point_2, p2_apex_value_2+0.1, lower_boundary, upper_boundary)


    print "In the first iteration we calculate the value function and stopping set for the player 1.\n"
    y_points = np.array([p1_routines.stop_first_payoff(x, *p1_stop_first_payoff_args) for x in x_points])
    
    p2_stopping_set, p2_previous_stopping_set, p2_stop_in_interior = [], [], False
    p2_discretised_value_function = []
    y2_points = np.array([p2_routines.stop_first_payoff(x, *p2_stop_first_payoff_args) for x in x_points])
    p2_stop_second_points = np.array([p2_routines.stop_second_payoff(x, *p2_stop_second_payoff_args) for x in x_points])

    # First iteration, calculate the value function for P1 assuming P2 never stops in the interior of the state space
    p1_c_hull_0, p1_c_hull_1 = p1_routines.get_concave_biconjugate_p1(x_points, y_points)

    p1_stop_second_points = np.array([p1_routines.stop_second_payoff(x, *p1_stop_second_payoff_args) for x in x_points])

    p1_stopping_set, p1_stop_in_interior = p1_routines.get_discrete_stopping_set_p1(p1_c_hull_0, p1_c_hull_1,
                                                                                    p2_stopping_set, boundaries,
                                                                                    *p1_stop_first_payoff_args)
    
    if (p1_routines.stop_second_payoff(lower_boundary, *p1_stop_second_payoff_args) <= p1_routines.stop_first_payoff(lower_boundary, *p1_stop_first_payoff_args)) and (lower_boundary not in p1_stopping_set):
        p1_stopping_set.insert(0, lower_boundary)
    if (p1_routines.stop_second_payoff(upper_boundary, *p1_stop_second_payoff_args) <= p1_routines.stop_first_payoff(upper_boundary, *p1_stop_first_payoff_args)) and (upper_boundary not in p1_stopping_set):
        p1_stopping_set.append(upper_boundary)
    
    p1_previous_stopping_set = list(p1_stopping_set)

    p1_discretised_value_function = np.vstack((p1_c_hull_0, p1_c_hull_1)).T

    num_iterations = 1000
    doPlotValueFunction, doFinalValueFunctionPlot, doSaveValueFunctionPlot = True, True, False
    doSaveFinalValueFunctionPlot, doFinalTransformed_VFPlot, doSaveTransformedVFPlot = True, True, True
    this_iteration = 0

    for this_iteration in xrange(2, num_iterations+1):
        print "This is iteration %d and Player %d's turn.\n" % (this_iteration, 2 - (this_iteration % 2))
        if this_iteration % 2 == 0:
            if p1_stop_in_interior:
                print "Player 1 stopped in the interior of the state space during the previous iteration, so we calculate the value function for the stopped process.\n"
                p1_stopping_set_and_boundaries = list(p1_stopping_set)
                if lower_boundary not in p1_stopping_set_and_boundaries:
                    p1_stopping_set_and_boundaries.insert(0, lower_boundary)
                if upper_boundary not in p1_stopping_set_and_boundaries:
                    p1_stopping_set_and_boundaries.append(upper_boundary)

                p2_c_hull_0, p2_c_hull_1, p2_discretised_value_function, p2_discretised_transformed_payoff = \
                    p2_routines.get_concave_biconjugate_stopped_process_p2(x_points, p1_stopping_set,
                                                                           p1_stopping_set_and_boundaries, dx,
                                                                           p2_stop_first_payoff_args,
                                                                           p2_stop_second_payoff_args)

            else:
                print "Player 1 did not stop in the interior of the state space during the previous iteration, so we calculate the value function for the entire state space.\n"
                # note that this can be calculated just once before the iterations
                p2_c_hull_0, p2_c_hull_1 = p2_routines.get_concave_biconjugate_p2(x_points, y2_points)
                p2_discretised_value_function = np.vstack((p2_c_hull_0, p2_c_hull_1)).T
                # plot function and concave biconjugate

            if doPlotValueFunction:
                plt.figure()
                plt.plot(x_points, y2_points, color='b', linestyle='--')
                plt.plot(x_points, y_points, color='b')
                plt.plot(x_points, p1_stop_second_points, color='g')
                plt.plot(x_points, p2_stop_second_points, color='g', linestyle='--')
                plt.plot(p2_discretised_value_function[:, 0], p2_discretised_value_function[:, 1], color='r', linestyle='--')
                plt.plot(p1_discretised_value_function[:, 0], p1_discretised_value_function[:, 1], color='r')
                plt.show()

            p2_stopping_set, p2_stop_in_interior = p2_routines.get_discrete_stopping_set_p2(p2_c_hull_0, p2_c_hull_1,
                                                                                      p1_stopping_set, boundaries,
                                                                                      *p2_stop_first_payoff_args)

            if (p2_routines.stop_second_payoff(lower_boundary, *p2_stop_second_payoff_args) <= p2_routines.stop_first_payoff(lower_boundary, *p2_stop_first_payoff_args)) and (lower_boundary not in p2_stopping_set):
                p2_stopping_set.insert(0, lower_boundary)
            if (p2_routines.stop_second_payoff(upper_boundary, *p2_stop_second_payoff_args) <= p2_routines.stop_first_payoff(upper_boundary, *p2_stop_first_payoff_args)) and (upper_boundary not in p2_stopping_set):
                p2_stopping_set.append(upper_boundary)

            print "There were %d elements in Player 2's stopping set and now there are %d elements.\n" % (
            len(p2_previous_stopping_set), len(p2_stopping_set))

            print "Does P2 stop in the interior of the state space?", p2_stop_in_interior, '\n'
            if np.array_equal(p2_previous_stopping_set, p2_stopping_set):
                print 'The algorithm has terminated at iteration %d.\n' % this_iteration
                break
            else:
                p2_previous_stopping_set = list(p2_stopping_set)
        else:
            if p2_stop_in_interior:
                print "Player 2 stopped in the interior of the state space during the previous iteration, so we calculate the value function for the stopped process.\n"
                p2_stopping_set_and_boundaries = list(p2_stopping_set)
                if lower_boundary not in p2_stopping_set_and_boundaries:
                    p2_stopping_set_and_boundaries.insert(0, lower_boundary)
                if upper_boundary not in p2_stopping_set_and_boundaries:
                    p2_stopping_set_and_boundaries.append(upper_boundary)

                p1_c_hull_0, p1_c_hull_1, p1_discretised_value_function, p1_discretised_transformed_payoff = p1_routines.get_concave_biconjugate_stopped_process_p1(
                    x_points, p2_stopping_set, p2_stopping_set_and_boundaries, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args)

            else:
                print "Player 2 did not stop in the interior of the state space during previous iteration, so we calculate the value function for the entire state space.\n"
                # note that this can be calculated just once before the iterations
                p1_c_hull_0, p1_c_hull_1 = p1_routines.get_concave_biconjugate_p1(x_points, y_points)
                p1_discretised_value_function = np.vstack((p1_c_hull_0, p1_c_hull_1)).T
                # plot function and convave biconjugate
            if doPlotValueFunction:
                plt.figure()
                plt.plot(x_points, y2_points, color='b', linestyle='--')
                plt.plot(x_points, y_points, color='b')
                plt.plot(x_points, p1_stop_second_points, color='g')
                plt.plot(x_points, p2_stop_second_points, color='g', linestyle='--')
                plt.plot(p2_discretised_value_function[:, 0], p2_discretised_value_function[:, 1], color='r',
                         linestyle='--')
                plt.plot(p1_discretised_value_function[:, 0], p1_discretised_value_function[:, 1], color='r')
                plt.show()

            p1_stopping_set, p1_stop_in_interior = p1_routines.get_discrete_stopping_set_p1(p1_c_hull_0, p1_c_hull_1, p2_stopping_set,
                                                                                      boundaries, *p1_stop_first_payoff_args)

            if (p1_routines.stop_second_payoff(lower_boundary, *p1_stop_second_payoff_args) <= p1_routines.stop_first_payoff(lower_boundary, *p1_stop_first_payoff_args)) and (lower_boundary not in p1_stopping_set):
                p1_stopping_set.insert(0, lower_boundary)
            if (p1_routines.stop_second_payoff(upper_boundary, *p1_stop_second_payoff_args) <= p1_routines.stop_first_payoff(upper_boundary, *p1_stop_first_payoff_args)) and (upper_boundary not in p1_stopping_set):
                p1_stopping_set.append(upper_boundary)
            
            print "There were %d elements in Player 1's stopping set and now there are %d elements.\n" % (
            len(p1_previous_stopping_set), len(p1_stopping_set))
            
            print "Does P1 stop in the interior of the state space?", p1_stop_in_interior, '\n'
            if np.array_equal(p1_stopping_set, p1_previous_stopping_set):
                print 'The algorithm has terminated at iteration %d.\n' % this_iteration
                break
            else:
                p1_previous_stopping_set = list(p1_stopping_set)

    print "Player 1's stopping set is ", p1_stopping_set
    print "Player 2's stopping set is", p2_stopping_set
    
    if doFinalValueFunctionPlot:
        plt.figure()
        title = "Approximate solution to the nonzero-sum optimal stopping game after "+str(this_iteration)+" iterations"
        plt.title('\n'.join(wrap(title, 70)) + "\n")
        plt.tight_layout()
        ax = plt.subplot(111)
        ax.plot(x_points, y2_points, color='b', label=r"$x \mapsto f_{2}(x)$",  linestyle='-', markevery=len(x_points)/10)
        ax.plot(x_points, y_points, color='r', label=r"$x \mapsto f_{1}(x)$",  linestyle='-', markevery=len(x_points)/10)
        ax.plot(x_points, p1_stop_second_points, color='r', label=r"$x \mapsto g_{1}(x)$",  linestyle='-.', markevery=len(x_points)/10)
        ax.plot(x_points, p2_stop_second_points, color='b', label=r"$x \mapsto g_{2}(x)$",  linestyle='-.', markevery=len(x_points)/10)
        ax.plot(p2_discretised_value_function[:, 0], p2_discretised_value_function[:, 1], color='b', linestyle='--', label=r"$x \mapsto V_{2}(x)$", markevery=len(x_points)/10)
        ax.plot(p1_discretised_value_function[:, 0], p1_discretised_value_function[:, 1], color='r', linestyle='--', label=r"$x \mapsto V_{1}(x)$", markevery=len(x_points)/10)
        
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveFinalValueFunctionPlot:
            plt.savefig('./value_functions.eps', bbox_inches='tight',
                        format='eps', dpi=1000)
            plt.close()
        else:
            plt.show()

    if doFinalTransformed_VFPlot:

        p1_c_hull_0, p1_c_hull_1, p1_discretised_value_function, p1_discretised_transformed_payoff = p1_routines.get_concave_biconjugate_stopped_process_p1(
            x_points, p2_stopping_set, p2_stopping_set, dx, p1_stop_first_payoff_args,
            p1_stop_second_payoff_args)

        p1_discretised_transformed_obstacle = y_points[np.argwhere(np.in1d(x_points,
                                                                       p1_discretised_value_function[:,
                                                                       0].ravel())).ravel()] - p1_discretised_transformed_payoff[
                                                                                               :,
                                                                                               1].ravel()
        p1_discretised_transformed_vf = p1_discretised_value_function[:, 1].ravel() - p1_discretised_transformed_payoff[:,
                                                                                 1].ravel()

        p2_c_hull_0, p2_c_hull_1, p2_discretised_value_function, p2_discretised_transformed_payoff = \
            p2_routines.get_concave_biconjugate_stopped_process_p2(x_points, p1_stopping_set,
                                                                   p1_stopping_set, dx,
                                                                   p2_stop_first_payoff_args,
                                                                   p2_stop_second_payoff_args)

        p2_discretised_transformed_obstacle = y2_points[np.argwhere(np.in1d(x_points,
                                                                        p2_discretised_value_function[:,
                                                                        0].ravel())).ravel()] - p2_discretised_transformed_payoff[
                                                                                                :, 1].ravel()
        # p2_discretised_transformed_payoff means transformed stop second payoff
        p2_discretised_transformed_vf = p2_discretised_value_function[:, 1].ravel() - p2_discretised_transformed_payoff[
                                                                                 :, 1].ravel()

        plt.figure()
        title = "Approximate solution to the nonzero-sum optimal stopping game after " + str(
           this_iteration) + " iterations"
        plt.title('\n'.join(wrap(title, 70)) + "\n")
        plt.tight_layout()
        ax = plt.subplot(111)
        ax.plot(p1_discretised_value_function[:, 0], p1_discretised_transformed_obstacle, color='r',
                        linestyle='-', label='$x \mapsto [f_{1} - g_{1,A_{' + str(this_iteration - 1) + '}}](x)$', markevery=len(x_points) / 10)
        ax.plot(p1_discretised_value_function[:, 0].ravel(), p1_discretised_transformed_vf, color='r',
                        linestyle='--', label="$x \mapsto [V_{1}^{A_{" + str(this_iteration - 1) + "}} - g_{1,A_{" + str(
                        this_iteration - 1) + '}}](x)$', markevery=len(x_points) / 10)
        ax.plot(p2_discretised_value_function[:, 0], p2_discretised_transformed_obstacle, color='b',
                        linestyle='-', label='$x \mapsto [f_{2} - g_{2,A_{' + str(this_iteration) + '}}](x)$', markevery=len(x_points) / 10)
        ax.plot(p2_discretised_value_function[:, 0].ravel(), p2_discretised_transformed_vf, color='b',
                        linestyle='--', label="$x \mapsto [V_{2}^{A_{" + str(this_iteration) + "}} - g_{2,A_{" + str(
                        this_iteration) + '}}](x)$', markevery=len(x_points) / 10)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveTransformedVFPlot:
            plt.savefig('./transformed_value_functions.eps', bbox_inches='tight',
                        format='eps', dpi=1000)
            plt.close()
        else:
            plt.show()