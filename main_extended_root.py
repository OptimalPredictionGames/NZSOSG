from common_imports import *
# python setup.py build_ext --inplace
from scipy.misc import derivative as scipy_derivative
from scipy.optimize import minimize, basinhopping


if __name__ == '__main__':
    import p1_routines_quartic as p1_routines
    import p2_routines_quartic as p2_routines

    def feasibility_penalty(a, b, c, derivative_roots):
        if (b < c) and (derivative_roots[0] <= a <= b <= derivative_roots[1]) and (c >= derivative_roots[2]):
            return 0.0
        else:
            return 1.0

    def p1_transformed_expected_threshold_reward(x, a, b, c, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        if x >= c:
            return 0.0
        if a <= x <= b:
            f_x = p1_routines.stop_first_payoff(x, *p1_stop_first_payoff_args)
            g_c_x = p1_follower_transformed(x, c, *p1_stop_second_payoff_args)
            return f_x - g_c_x
        if x < a:
            f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
            g_c_a = p1_follower_transformed(a, c, *p1_stop_second_payoff_args)
            return (f_a - g_c_a) * (x / a)

        f_b = p1_routines.stop_first_payoff(b, *p1_stop_first_payoff_args)
        g_c_b = p1_follower_transformed(b, c, *p1_stop_second_payoff_args)
        return (f_b - g_c_b) * (c - x) / (c - b)


    def p2_transformed_expected_threshold_reward(y, a, b, c, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        if y <= b:
            return 0.0
        if y >= c:
            f_y = p2_routines.stop_first_payoff(y, *p2_stop_first_payoff_args)
            g_a_b_y = p2_follower_transformed(a, b, y, *p2_stop_second_payoff_args)
            return f_y - g_a_b_y
        f_c = p2_routines.stop_first_payoff(c, *p2_stop_first_payoff_args)
        g_a_b_c = p2_follower_transformed(a, b, c, *p2_stop_second_payoff_args)
        return (f_c - g_a_b_c) * (y - b) / (c-b)


    def p1_follower_transformed(a, b, *p1_stop_second_payoff_args):
        if a >= b:
            return p1_routines.stop_second_payoff(a, *p1_stop_second_payoff_args)
        return p1_routines.stop_second_payoff(b, *p1_stop_second_payoff_args) * (a/b)


    def p2_follower_transformed(a, b, c, *p2_stop_second_payoff_args):
        if a <= c <= b:
            return p2_routines.stop_second_payoff(c, *p2_stop_second_payoff_args)
        elif c > b:
            return p2_routines.stop_second_payoff(b, *p2_stop_second_payoff_args) * ((1-c)/(1-b))
        else:
            return p2_routines.stop_second_payoff(a, *p2_stop_second_payoff_args) * (c / a)

    def p1_utility_function_1(a, b, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        if a >= b:
            return -np.inf
        if a > 0:
            f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
            g_b_a = p1_follower_transformed(a, b, *p1_stop_second_payoff_args)
            utility_value = (f_a - g_b_a)/a
        else:
            utility_value = scipy_derivative(p1_routines.stop_first_payoff, a, dx=dx, args=p1_stop_first_payoff_args) - p1_routines.stop_second_payoff(b, *p1_stop_second_payoff_args)/b
        return utility_value

    def p1_utility_function_2(a, b, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        if a >= b:
            return -np.inf
        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        g_b_a = p1_follower_transformed(a, b, *p1_stop_second_payoff_args)
        utility_value = (f_a - g_b_a)/(b-a)
        return utility_value

    def p2_utility_function(a, b, c, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        if b >= c or c <= a:
            return -np.inf
        f_c = p2_routines.stop_first_payoff(c, *p2_stop_first_payoff_args)
        g_a_b_c = p2_follower_transformed(a, b, c, *p2_stop_second_payoff_args)
        utility_value = (f_c - g_a_b_c)/(c-b)
        return utility_value


    def kappa_function(a, dx, p1_stop_first_payoff_args):
        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        d_f_a = scipy_derivative(p1_routines.stop_first_payoff, a, dx=dx, args=p1_stop_first_payoff_args)
        non_linear_value = (d_f_a * a) - f_a
        return non_linear_value

    def theta_function(a, b, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args):
        f_a = p1_routines.stop_first_payoff(a, *p1_stop_first_payoff_args)
        d_f_a = scipy_derivative(p1_routines.stop_first_payoff, a, dx=dx, args=p1_stop_first_payoff_args)
        g_b = p1_routines.stop_second_payoff(b, *p1_stop_second_payoff_args)
        non_linear_value = f_a + (d_f_a*(b - a)) - g_b
        return non_linear_value

    def gamma_function(a, b, dx, p2_stop_first_payoff_args, p2_stop_second_payoff_args):
        f_b = p2_routines.stop_first_payoff(b, *p2_stop_first_payoff_args)
        d_f_b = scipy_derivative(p2_routines.stop_first_payoff, b, dx=dx, args=p2_stop_first_payoff_args)
        g_a = p2_routines.stop_second_payoff(a, *p2_stop_second_payoff_args)
        non_linear_value = f_b + (d_f_b*(a - b)) - g_a
        return non_linear_value

    def triple_smooth_fit(guess, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args, p2_stop_first_payoff_args,
                          p2_stop_second_payoff_args, derivative_roots):

        a, b, c = guess
        value = kappa_function(a, dx, p1_stop_first_payoff_args)**2
        value += theta_function(b, c, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args)**2
        value += gamma_function(b, c, dx, p2_stop_first_payoff_args, p2_stop_second_payoff_args)**2
        value += feasibility_penalty(a, b, c, derivative_roots)
        return value

    lower_boundary = 0.0
    upper_boundary = 1.0
    boundaries = [lower_boundary, upper_boundary]
    num_points = 10000
    dx = (upper_boundary - lower_boundary) / float(num_points)
    x_points = np.arange(lower_boundary, upper_boundary+dx, dx)

    # quartic case
    second_derivative_quadratic_coef, second_derivative_root_1, second_derivative_root_2, second_derivative_constant, \
    constant_term = 50.0, 0.1, 0.6, 0.0, 0.0

    p1_stop_first_payoff_args = (second_derivative_quadratic_coef, second_derivative_root_1, second_derivative_root_2,
                                 second_derivative_constant, constant_term)

    stop_second_constant_term = constant_term
    stop_second_extra_quad_coef = 0.25

    p1_stop_second_payoff_args = (second_derivative_quadratic_coef, second_derivative_root_1, second_derivative_root_2,
                                  second_derivative_constant, stop_second_constant_term, lower_boundary, upper_boundary,
                                  stop_second_extra_quad_coef)

    p2_second_derivative_slope, p2_second_derivative_zero, p2_constant_term = 15.0, 0.625, 0.0
    p2_stop_first_payoff_args = (p2_second_derivative_slope, 1.0-p2_second_derivative_zero, p2_constant_term)
    p2_stop_second_constant_term = p2_constant_term
    p2_stop_second_extra_quad_coef = 0.25

    derivative_roots = [second_derivative_root_1, second_derivative_root_2, p2_second_derivative_zero]

    p2_stop_second_payoff_args = (p2_second_derivative_slope, 1.0 - p2_second_derivative_zero, p2_stop_second_constant_term, lower_boundary,
                                  upper_boundary, p2_stop_second_extra_quad_coef)

    y_points = np.array([p1_routines.stop_first_payoff(x, *p1_stop_first_payoff_args) for x in x_points])
    p1_stop_second_points = np.array([p1_routines.stop_second_payoff(x, *p1_stop_second_payoff_args) for x in x_points])
    y2_points = np.array([p2_routines.stop_first_payoff(x, *p2_stop_first_payoff_args) for x in x_points])
    p2_stop_second_points = np.array([p2_routines.stop_second_payoff(x, *p2_stop_second_payoff_args) for x in x_points])

    thresholds = np.array([0.16012376, 0.43412365, 0.80982308], dtype=np.float64)

    doPlotRewardFunction = True
    doSaveRewardFunctionPlot = True

    doPlotUtilityFunction = False
    doSaveUtilityFunctionPlot = True

    doSolveGame = True
    doBasinHopping = True
    doPlotValueFunction = True
    doSaveValueFunctionPlot = True

    if doSolveGame:
        if doBasinHopping:
            result = basinhopping(triple_smooth_fit, thresholds, niter=100000,
                                        minimizer_kwargs={
                                            'method': 'SLSQP',
                                            'bounds': [(second_derivative_root_1, second_derivative_root_2),
                                                      (second_derivative_root_1, second_derivative_root_2),
                                                      (p2_second_derivative_zero, upper_boundary)],
                                             'args': (dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args,
                                                      p2_stop_first_payoff_args, p2_stop_second_payoff_args,
                                                      derivative_roots),
                                             'options': {'ftol': 1e-12}
                                        }
                                  )
        else:
            result = minimize(triple_smooth_fit, thresholds, method='L-BFGS-B',
                              bounds=[(second_derivative_root_1, second_derivative_root_2),
                                      (second_derivative_root_1, second_derivative_root_2),
                                      (p2_second_derivative_zero, upper_boundary)],
                              args=(dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args, p2_stop_first_payoff_args,
                                    p2_stop_second_payoff_args, derivative_roots), options={'ftol': 10.0})
        if not doBasinHopping:
            print "Success?: ", result.success
            if not result.success:
                print result.message

        thresholds = result.x
        print thresholds, result.fun

    if doPlotRewardFunction:
        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points, color='r', label=r"$x \mapsto f_{1}(x)$",
                markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, p1_stop_second_points, color='r', label=r"$x \mapsto g_{1}(x)$",
                linestyle='-.',markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, y2_points, color='b', label=r"$x \mapsto f_{2}(x)$",
                markevery=len(x_points) / 10, linewidth=2.0)
        ax.plot(x_points, p2_stop_second_points, color='b', label=r"$x \mapsto g_{2}(x)$",
                linestyle='-.',markevery=len(x_points) / 10, linewidth=2.0)

        ax.axvline(second_derivative_root_1, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(second_derivative_root_2, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=1.5)

        ax.annotate(r"${a_{1}}$", xy=(second_derivative_root_1, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${a_{2}}$", xy=(second_derivative_root_2, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(+6, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveRewardFunctionPlot:
            plt.savefig('./results/reward_functions_quartic.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()

    if doPlotUtilityFunction:
        x, y, z = thresholds
        p1_follower_transformed_values = np.array(
            [p1_follower_transformed(x_, z, *p1_stop_second_payoff_args) for x_ in x_points])

        p1_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) <= p2_second_derivative_zero)]
        p1_utilities_1 = np.array(
            [p1_utility_function_1(x_, z, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in p1_state_space])
        p1_utilities_2 = np.array(
            [p1_utility_function_2(x_, z, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in p1_state_space])
        p2_follower_transformed_values = np.array(
            [p2_follower_transformed(x, y, z_, *p2_stop_second_payoff_args) for z_ in x_points])
        p2_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) >= second_derivative_root_2)]
        p2_utilities = np.array(
            [p2_utility_function(x, y, z_, p2_stop_first_payoff_args, p2_stop_second_payoff_args) for z_ in p2_state_space])

        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points-p1_follower_transformed_values, color='r', label=r"$x \mapsto f_{1}(x) - g_{1,[\hat{z},1]}(x)$", marker='*', markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(p1_state_space, p1_utilities_1, color='r', markevery=len(x_points) / 10, marker='d',
                linewidth=2.0, label="$x \mapsto U_{1}(x,\hat{y},\hat{z})$")

        ax.plot(p1_state_space, p1_utilities_2, color='r', markevery=len(x_points) / 10, marker='D',
                linewidth=2.0, label="$y \mapsto U_{2}(\hat{x},y,\hat{z})$")

        ax.plot(x_points, y2_points-p2_follower_transformed_values, color='b', label=r"$z \mapsto f_{2}(z) - g_{2,[0,\hat{x}]}(z)$", marker='*', markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(p2_state_space, p2_utilities, color='b', markevery=len(x_points) / 10, marker='d',
                linewidth=2.0, label="$z \mapsto U_{3}(\hat{x},\hat{y},z)$")

        ax.axvline(x, linestyle=':', color='k', linewidth=2.0)
        ax.axvline(y, linestyle=':', color='k', linewidth=2.0)
        ax.axvline(z, linestyle=':', color='k', linewidth=2.0)

        ax.annotate(r"${\hat{x}}$", xy=(x, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${\hat{y}}$", xy=(y, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.annotate(r"${\hat{z}}$", xy=(z, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.axvline(second_derivative_root_1, linestyle='--', color='k', linewidth=2.0)
        ax.axvline(second_derivative_root_2, linestyle='--', color='k', linewidth=2.0)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=2.0)

        ax.annotate(r"${a_{1}}$", xy=(second_derivative_root_1, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${a_{2}}$", xy=(second_derivative_root_2, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(+6, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveUtilityFunctionPlot:
            plt.savefig('./results/utility_functions_quartic.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()

    if doPlotValueFunction:
        x, y, z = thresholds

        p1_transformed_expected_threshold_reward_values = np.array([p1_transformed_expected_threshold_reward(x_, x, y, z,
                                                                                                             p1_stop_first_payoff_args,
                                                                                                             p1_stop_second_payoff_args)
                                                                    for x_ in x_points])

        p1_follower_transformed_values = np.array(
            [p1_follower_transformed(x_, z, *p1_stop_second_payoff_args) for x_ in x_points])

        p1_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) <= p2_second_derivative_zero)]
        p1_utilities_1 = np.array(
            [p1_utility_function_1(x_, z, dx, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in
             p1_state_space])
        p1_utilities_2 = np.array(
            [p1_utility_function_2(x_, z, p1_stop_first_payoff_args, p1_stop_second_payoff_args) for x_ in
             p1_state_space])

        p2_transformed_expected_threshold_reward_values = np.array([p2_transformed_expected_threshold_reward(y_, x, y, z,
                                                                                                             p2_stop_first_payoff_args,
                                                                                                             p2_stop_second_payoff_args)
                                                                    for y_ in x_points])
        p2_follower_transformed_values = np.array(
            [p2_follower_transformed(x, y, z_, *p2_stop_second_payoff_args) for z_ in x_points])
        p2_state_space = np.asarray(x_points)[np.where(np.asarray(x_points) >= second_derivative_root_2)]
        p2_utilities = np.array(
            [p2_utility_function(x, y, z_, p2_stop_first_payoff_args, p2_stop_second_payoff_args) for z_ in
             p2_state_space])

        plt.figure()
        ax = plt.subplot(111)
        ax.plot(x_points, y_points - p1_follower_transformed_values, color='r',
                label=r"$x \mapsto f_{1}(x) - g_{1,[z_{*},1]}(x)$",
                markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(x_points, p1_transformed_expected_threshold_reward_values, color='r',
                label=r"$x \mapsto \tilde{V}_{1}^{[z_{*},1]}(x)$",
                linestyle='--', markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(p1_state_space, p1_utilities_1, color='r', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$x \mapsto U_{1}(x,y_{*},z_{*})$")

        ax.plot(p1_state_space, p1_utilities_2, color='m', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$y \mapsto U_{2}(x_{*},y,z_{*})$")

        ax.plot(x_points, y2_points - p2_follower_transformed_values, color='b',
                label=r"$z \mapsto f_{2}(z) - g_{2,[x_{*},y_{*}]}(z)$",
                markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(x_points, p2_transformed_expected_threshold_reward_values, color='b',
                label=r"$z \mapsto \tilde{V}_{2}^{[x_{*},y_{*}]}(z)$",
                linestyle='--',markevery=len(x_points) / 10,
                linewidth=2.0)

        ax.plot(p2_state_space, p2_utilities, color='b', markevery=len(x_points) / 10,
                linestyle='-.', linewidth=2.0, label="$z \mapsto U_{3}(x_{*},y_{*},z)$")

        ax.axvline(x, linestyle=':', color='k', linewidth=2.0)
        ax.axvline(y, linestyle=':', color='k', linewidth=2.0)
        ax.axvline(z, linestyle=':', color='k', linewidth=2.0)

        ax.annotate(r"${x_{*}}$", xy=(x, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${y_{*}}$", xy=(y, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.annotate(r"${z_{*}}$", xy=(z, ax.get_ylim()[0]), xytext=(-7, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        ax.axvline(second_derivative_root_1, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(second_derivative_root_2, linestyle='--', color='k', linewidth=1.5)
        ax.axvline(p2_second_derivative_zero, linestyle='--', color='k', linewidth=1.5)

        ax.annotate(r"${a_{1}}$", xy=(second_derivative_root_1, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${a_{2}}$", xy=(second_derivative_root_2, ax.get_ylim()[0]), xytext=(-8, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')
        ax.annotate(r"${b}$", xy=(p2_second_derivative_zero, ax.get_ylim()[0]), xytext=(+6, 10),
                    textcoords='offset points', color='k', weight='extra bold', size='large', ha='center')

        plt.tight_layout()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doSaveValueFunctionPlot:
            plt.savefig('./value_functions_quartic.eps', bbox_inches='tight',
                        format='eps', dpi=300)
            plt.close()
        else:
            plt.show()