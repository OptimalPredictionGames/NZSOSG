from common_imports import *

cimport cython
from libc.math cimport exp


cpdef double standard_logistic(double y):
    cdef double value = 1.0 / (1 + exp(-y))
    return value


cpdef double interval_indicator(double x, double a, double b, double steepness):
    cdef double indicator = standard_logistic(steepness*(x-a)) * standard_logistic(steepness*(b-x))
    return indicator


cpdef double puzzle_stop_first_smooth(double x, double impulse_point, double steepness):
    return interval_indicator(x, impulse_point, impulse_point, steepness)


def stop_first_payoff(double x, *args):
    cdef double result = puzzle_stop_first_smooth(x, *args)
    return result


cpdef double puzzle_stop_second(double x, double apex_point, double apex_value, double lower_boundary, double upper_boundary):
    cdef double result
    if (lower_boundary <= x <= apex_point):
        result = apex_value * (x - lower_boundary) / (apex_point - lower_boundary)
    elif (apex_point < x <= upper_boundary):
        result = apex_value * (upper_boundary - x) / (upper_boundary - apex_point)
    else:
        result = 0.0
    return result


def stop_second_payoff(double x, *args):
    cdef double result = puzzle_stop_second(x, *args)
    return result


def line_segment(double x, double slope, double x_1, double y_1):
    cdef double result = y_1 + (slope * (x - x_1))
    return result

# returns the index of the greatest point in the partition of an interval which lies below a desired point in that interval
def find_nearest_index(double x, double lower_boundary, double upper_boundary, int num_points):
    cdef double _lambda = (x - lower_boundary) / (upper_boundary - lower_boundary)
    cdef int result = int(num_points * _lambda)
    return result

# projection operator, Definition 7 of Helluy and Mathis (2010)
def projected_payoff(double x, double lower_boundary, double upper_boundary, int num_points, points, *args):
    cdef double result, y_2, y_1, x_2, x_1
    cdef int nearest_point_index
    if (x < lower_boundary) or (x > upper_boundary):
        return np.inf
    else:
        if (x == upper_boundary):
            result = stop_first_payoff(upper_boundary, *args)
        elif (x == lower_boundary):
            result = stop_first_payoff(lower_boundary, *args)
        else:
            nearest_point_index = find_nearest_index(x, lower_boundary, upper_boundary, num_points)
            x_2 = points[nearest_point_index + 1]
            x_1 = points[nearest_point_index]
            y_2 = stop_first_payoff(x_2, *args)
            y_1 = stop_first_payoff(x_1, *args)
            result =  y_1 + ((y_2 - y_1) * ((x-x_1)/(x_2 - x_1)))
        return result

# computation of convex hull following Helluy and Mathis (2010)
# I don't see the point of the projection since I am effectively evaluating the interpolant at the interpolating points!
# Pi[f](x) = f(x) for every x in the set of interpolating points.

def graph_slope(P1, P2):
    cdef double result, x_1, x_2, f_1, f_2
    x_1 = P1[0]
    x_2 = P2[0]
    f_1 = P1[1] #projected_payoff(x_1, lower_boundary, upper_boundary, num_points, points)
    f_2 = P2[1] #projected_payoff(x_2, lower_boundary, upper_boundary, num_points, points)
    result = (f_1 - f_2) / (x_1 - x_2)
    return result

def convex_hull(graph):
    cdef bint remove = True
    cdef double slope_1, slope_2
    cdef int i
    while (len(graph) > 2 and remove):
        remove = False
        i = 1
        remove_indices = []
        for i in xrange(1,len(graph)-1):
            slope_1 = graph_slope(graph[i-1], graph[i])
            slope_2 = graph_slope(graph[i], graph[i+1])
            if (slope_1 < slope_2):
                remove_indices.append(i)
        if len(remove_indices) > 0:
            remove = True
            for index in remove_indices[::-1]:
                del graph[index]
    return graph

def get_concave_biconjugate_p1(x_points, y_points):
    # concave biconjugate can be obtained simply by taking discrete convex hull of the graph of the function
    # the convex_hull function returns the points in the stopping region
    # continuation region is a line segment joining disconnected stopping sets in a contiguous way
    graph_points = zip(x_points, y_points)
    c_hull = convex_hull(graph_points)
    c_hull_0, c_hull_1 = np.array(zip(*c_hull))
    return c_hull_0, c_hull_1

def get_concave_biconjugate_stopped_process_p1(x_points, p2_stopping_set, p2_stopping_set_and_boundaries, double dx, stop_first_payoff_args, stop_second_payoff_args):
    transformed_stop_second_payoff = np.array([stop_second_payoff(x, *stop_second_payoff_args) for x in p2_stopping_set_and_boundaries])
    c_hull_0 = []
    c_hull_1 = []
    transformed_payoff = []
    for i in xrange(1, len(p2_stopping_set_and_boundaries)):
        # if we skip a step then we're in the continuation region
        if (p2_stopping_set_and_boundaries[i] - p2_stopping_set_and_boundaries[i - 1]) > 1.25 * dx:
            indices = np.argwhere((x_points >= p2_stopping_set_and_boundaries[i - 1]) & (x_points <= p2_stopping_set_and_boundaries[i])).ravel()
            transformation_slope = (transformed_stop_second_payoff[i] - transformed_stop_second_payoff[i - 1]) / (
            p2_stopping_set_and_boundaries[i] - p2_stopping_set_and_boundaries[i - 1])
            stop_first_values, new_transformed_payoff = np.array(zip(*[(stop_first_payoff(x, *stop_first_payoff_args),
                                          line_segment(x, transformation_slope, p2_stopping_set_and_boundaries[i - 1],
                                                                   transformed_stop_second_payoff[i - 1]))
                                      for x in x_points[indices]]))
            new_y2_points = np.maximum(stop_first_values, new_transformed_payoff)
            new_graph_points = zip(x_points[indices], new_y2_points)
            new_convex_hull = convex_hull(new_graph_points)
            new_c_hull_0, new_chull_1 = list(zip(*new_convex_hull))
            c_hull_0 += new_c_hull_0
            c_hull_1 += new_chull_1
            new_transformed_payoff = list(new_transformed_payoff[np.argwhere(np.in1d(x_points[indices], new_c_hull_0)).ravel()])
            transformed_payoff += new_transformed_payoff

    c_hull_0, c_hull_1 = np.array(c_hull_0), np.array(c_hull_1)
    transformed_payoff = np.array(transformed_payoff)
    # get the discretised value function
    # we know its value on p1's stopping set
    value_function_on_p2_stopping_set = np.array([stop_second_payoff(x, *stop_second_payoff_args) for x in p2_stopping_set])
    transformed_payoff_on_p2_stopping_set = np.empty_like(value_function_on_p2_stopping_set)
    transformed_payoff_on_p2_stopping_set[:] = value_function_on_p2_stopping_set
    # outside of this stopping set we assign the convex hull values calculated above
    continuation_indices = np.argwhere(~np.in1d(c_hull_0, p2_stopping_set)).ravel()
    # stack them up into a 2-d array...
    p1_discretised_value_function = np.concatenate((np.vstack((p2_stopping_set, value_function_on_p2_stopping_set)).T,
                                                    np.vstack((c_hull_0[continuation_indices],
                                                               c_hull_1[continuation_indices])).T))
                                                               
    p1_discretised_transformed_payoff = np.concatenate((np.vstack((p2_stopping_set, transformed_payoff_on_p2_stopping_set)).T,
                                                    np.vstack((c_hull_0[continuation_indices],
                                                               transformed_payoff[continuation_indices])).T))
    # then sort in ascending order by x-axis value
    p1_discretised_value_function[:] = p1_discretised_value_function[p1_discretised_value_function[:, 0].argsort()]
    p1_discretised_transformed_payoff[:] = p1_discretised_transformed_payoff[p1_discretised_transformed_payoff[:, 0].argsort()]
    
    return c_hull_0, c_hull_1, p1_discretised_value_function, p1_discretised_transformed_payoff

def get_discrete_stopping_set_p1(c_hull_x, c_hull_y, p2_stopping_set, boundaries, *args):
    #evaluate function at convex_hull x points then get stopping region
    payoff_convex_hull = np.array([stop_first_payoff(x, *args) for x in c_hull_x])
    stopping_indices = np.argwhere((payoff_convex_hull - c_hull_y >= 0)).ravel()
    stop_in_interior = False
    discrete_stopping_set = []
    if len(stopping_indices) > 0:
        discrete_stopping_set = c_hull_x[stopping_indices]
        if any(~np.in1d(p2_stopping_set, boundaries)):
            discrete_stopping_set = discrete_stopping_set[np.argwhere(~np.in1d(discrete_stopping_set, p2_stopping_set)).ravel()]
        if any(~np.in1d(discrete_stopping_set, boundaries)):
            stop_in_interior = True
    return list(discrete_stopping_set), stop_in_interior